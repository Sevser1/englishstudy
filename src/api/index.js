export const availableExercise = () => new Promise(resolve => resolve({
  data: [{
    title: 'Учить слова',
    desc: 'Тут написана тематика слов',
    id: '1',
    type: 'LearnWords',
  }, {
    title: 'Учить слова',
    desc: 'Тут написана тематика слов',
    id: '1',
    type: 'IrregularVerbsTrain',
  }, {
    title: 'Видео и вопросы',
    desc: 'Тут написана тематика слов',
    id: '1',
    type: 'YouTubeAndSelect',
  }, {
    title: 'Неправильные глаголы',
    desc: 'Описание настоящего упражнения очень большой текст используется для разностороннего описания упражнения',
    id: '1',
    type: 'IrregularVerbs',
  }, {
    title: 'Драг и дроп',
    desc: 'Описание настоящего упражнения очень большой текст используется для разностороннего описания упражнения',
    id: '1',
    type: 'draganddrop',
  }, {
    title: 'свайп',
    desc: 'Описание настоящего упражнения очень большой текст используется для разностороннего описания упражнения',
    id: '2',
    type: 'swipe',
  }, {
    title: 'заголовок упражнения',
    desc: 'Описание настоящего упражнения очень большой текст используется для разностороннего описания упражнения',
    id: '3',
    type: 'swipe',
  }, {
    title: 'заголовок упражнения',
    desc: 'Описание настоящего упражнения очень большой текст используется для разностороннего описания упражнения',
    id: '4',
    type: 'swipe',
  }],
}));

export const requestDragAndDrop = () => new Promise(resolve => resolve({
  data: {
    wordColumn: [{
      label: 'some 1 label',
      id: '1',
    }, {
      label: 'some 2 label',
      id: '2',
    }, {
      label: 'some 3 label',
      id: '3',
    }, {
      label: 'some 4 label',
      id: '4',
    }],
    availableWords: [{
      label: 'some 1 word',
      id: '1',
    }, {
      label: 'some 2 word',
      id: '2',
    }, {
      label: 'some 3 word',
      id: '3',
    }, {
      label: 'some 4 word',
      id: '4',
    }],
  },
}));

export const requestVerbs = () => new Promise(resolve => resolve({
  data: {
    verbs: [{
      Infinitive: 'be',
      InfinitiveAnswer: 'be',
      Past: '',
      PastAnswer: 'was',
      PastParticiple: '',
      PastParticipleAnswer: 'been',
      disabled: 'Infinitive',
    }, {
      Infinitive: '',
      InfinitiveAnswer: 'do',
      Past: 'did',
      PastAnswer: 'did',
      PastParticiple: '',
      PastParticipleAnswer: 'done',
      disabled: 'Past',
    }],
  },
}));

export const requestVerbsTrain = () => new Promise(resolve => resolve({
  data: {
    verbs: [{
      Infinitive: 'be',
      InfinitiveAnswer: 'be',
      Past: 'was',
      PastAnswer: 'was',
      PastParticiple: 'been',
      PastParticipleAnswer: 'been',
    }, {
      Infinitive: 'do',
      InfinitiveAnswer: 'do',
      Past: 'did',
      PastAnswer: 'did',
      PastParticiple: 'done',
      PastParticipleAnswer: 'done',
    }, {
      Infinitive: 'do',
      InfinitiveAnswer: 'do',
      Past: 'did',
      PastAnswer: 'did',
      PastParticiple: 'done',
      PastParticipleAnswer: 'done',
    }, {
      Infinitive: 'do',
      InfinitiveAnswer: 'do',
      Past: 'did',
      PastAnswer: 'did',
      PastParticiple: 'done',
      PastParticipleAnswer: 'done',
    }, {
      Infinitive: 'do',
      InfinitiveAnswer: 'do',
      Past: 'did',
      PastAnswer: 'did',
      PastParticiple: 'done',
      PastParticipleAnswer: 'done',
    }],
  },
}));

export const requestWords = () => new Promise(resolve => resolve({
  data: {
    listWords: [{
      id: '1',
      english: 'good',
      russian: 'хороший',
    }, {
      id: '2',
      english: 'bad',
      russian: 'плохой',
    }, {
      id: '3',
      english: 'to be',
      russian: 'быть',
    }, {
      id: '4',
      english: 'to speak',
      russian: 'говорить',
    }],
  },
}));


export const requestSwipes = () => new Promise(resolve => resolve({
  data: {
    questions: [{
      label: 'Вопрос 1',
      id: 1,
    }, {
      label: 'Вопрос 2',
      id: 2,
    }, {
      label: 'Вопрос 3',
      id: 3,
    }, {
      label: 'Вопрос 3',
      id: 4,
    }],
  },
}));


export const requestYoutubeAndRadioButton = () => new Promise(resolve => resolve({
  data: {
    urlVideo: 'https://www.youtube.com/embed/tgbNymZ7vqY',
    questions: [{
      id: '1',
      value: '',
      answer: 'option1',
      label: 'какое то утверждение про видео',
      options: ['option1', 'option2', 'option3'],
    }, {
      id: '1',
      value: '',
      answer: 'option2',
      label: 'какое то второе утверждение про видео',
      options: ['option1', 'option2', 'option3'],
    }, {
      id: '1',
      value: '',
      answer: 'option2',
      label: 'какое то третье утверждение про видео',
      options: ['option1', 'option2', 'option3'],
    }, {
      id: '1',
      value: '',
      answer: 'option3',
      label: 'какое то третье утверждение про видео',
      options: ['option1', 'option2', 'option3'],
    }],
  },
}));

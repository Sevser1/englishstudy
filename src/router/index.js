import Vue from 'vue';
import Router from 'vue-router';
import MainComponent from '../components/MainComponent';
import Home from '../components/Home';
import ListExercise from '../components/ListExersise';
import SwipeOutToTrue from '../components/excersizes/SwipeOutToTrue';
import DragAndDrop from '../components/excersizes/DragAndDrop';
import IrregularVerbs from '../components/excersizes/IrregularVerbs';
import IrregularVerbsTrain from '../components/excersizes/IrregularVerbsTrain';
import LearnWords from '../components/excersizes/LearnWords';
import YouTubeAndSelect from '../components/excersizes/YouTubeAndSelect';

Vue.use(Router);

const router = new Router({
  mode: 'history',
  base: '/englishstudy/',
  routes: [
    {
      path: '/',
      redirect: '/Home',
    },
    {
      path: '/:component',
      component: MainComponent,
      children: [{
        path: '/Home',
        name: 'Home',
        props: true,
        component: Home,
        meta: {
          title: 'Домашняя страница',
          authRequired: false,
        },
      }, {
        path: '/Swipe/:id',
        name: 'Swipe',
        props: true,
        component: SwipeOutToTrue,
        meta: {
          title: 'Свайп к правде',
          authRequired: false,
        },
      }, {
        path: '/DragAndDrop/:id',
        name: 'DragAndDrop',
        props: true,
        component: DragAndDrop,
        meta: {
          title: 'Драг к знаниям',
          authRequired: false,
        },
      }, {
        path: '/IrregularVerbs/:id',
        name: 'IrregularVerbs',
        props: true,
        component: IrregularVerbs,
        meta: {
          title: 'Глаголы к действию',
          authRequired: false,
        },
      }, {
        path: '/IrregularVerbsTrain/:id',
        name: 'IrregularVerbsTrain',
        props: true,
        component: IrregularVerbsTrain,
        meta: {
          title: 'Глаголы к действию',
          authRequired: false,
        },
      }, {
        path: '/LearnWords/:id',
        name: 'LearnWords',
        props: true,
        component: LearnWords,
        meta: {
          title: 'Учим слова',
          authRequired: false,
        },
      }, {
        path: '/Exercise',
        name: 'Exercise',
        props: true,
        component: ListExercise,
        meta: {
          title: 'Список упражнений',
          authRequired: false,
        },
      }, {
        path: '/YouTubeAndSelect/:id',
        name: 'YouTubeAndSelect',
        props: true,
        component: YouTubeAndSelect,
        meta: {
          title: 'Ответы на вопросы',
          authRequired: false,
        },
      }],
    },
  ],
});

router.beforeEach((to, from, next) => {
  next();
});

export default router;

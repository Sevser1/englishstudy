import * as api from '../api';

export const availableExercise = ({ commit }, params) => {
  api.availableExercise(params)
    .then(({ data }) => {
      commit('UPDATE_AVAILABLE_EXERCISE', data);
    })
    .catch()
    .finally();
};
export const requestDragAndDrop = ({ commit }, params) => {
  api.requestDragAndDrop(params)
    .then(({ data }) => {
      commit('UPDATE_DRAG_AND_DROP', { data, params });
    })
    .catch()
    .finally();
};
export const requestVerbs = ({ commit }, params) => {
  api.requestVerbs(params)
    .then(({ data }) => {
      commit('UPDATE_VERBS', { data, params });
    })
    .catch()
    .finally();
};
export const requestVerbsTrain = ({ commit }, params) => {
  api.requestVerbsTrain(params)
    .then(({ data }) => {
      commit('UPDATE_VERBS_TRAIN', { data, params });
    })
    .catch()
    .finally();
};
export const requestWords = ({ commit }, params) => {
  api.requestWords(params)
    .then(({ data }) => {
      commit('UPDATE_WORDS', { data, params });
    })
    .catch()
    .finally();
};
export const requestSwipes = ({ commit }, params) => {
  api.requestSwipes(params)
    .then(({ data }) => {
      commit('UPDATE_SWIPES', { data, params });
    })
    .catch()
    .finally();
};
export const requestYoutubeAndRadioButton = ({ commit }, params) => {
  api.requestYoutubeAndRadioButton(params)
    .then(({ data }) => {
      commit('UPDATE_YOUTUBE_SWIPE', { data, params });
    })
    .catch()
    .finally();
};

export const updateExerciseStatus = ({ commit }, params) => {
  commit('UPDATE_EXERCISE_STATUS', params);
};

export const getExercise = state => state.exerciseList;
export const getDragAndDrop = state => state.dragAndDrop;
export const getVerbs = state => state.verbs;
export const getWords = state => state.words;
export const getVerbsTrain = state => state.verbsTrain;
export const getSwipes = state => state.swipes;
export const getYoutubeSwipe = state => state.youtubeSwipe;

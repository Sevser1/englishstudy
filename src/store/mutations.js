export const UPDATE_AVAILABLE_EXERCISE = (state, resp) => {
  state.exerciseList = resp.map((item) => {
    const currentExerciseInState = state.exerciseList
      .find(exercise => item.type.toLowerCase() === exercise.type.toLowerCase() && item.id === exercise.id) || {};
    return {
      ...item,
      ready: currentExerciseInState.ready || false,
      answer: currentExerciseInState.answer || null,
    };
  });
};

export const UPDATE_DRAG_AND_DROP = (state, { data, params }) => {
  state.dragAndDrop = {
    ...state.dragAndDrop,
    [params.id]: data,
  };
};
export const UPDATE_VERBS = (state, { data, params }) => {
  state.verbs = {
    ...state.verbs,
    [params.id]: data,
  };
};
export const UPDATE_VERBS_TRAIN = (state, { data, params }) => {
  state.verbsTrain = {
    ...state.verbs,
    [params.id]: data,
  };
};
export const UPDATE_WORDS = (state, { data, params }) => {
  state.words = {
    ...state.words,
    [params.id]: data,
  };
};
export const UPDATE_SWIPES = (state, { data, params }) => {
  state.swipes = {
    ...state.swipes,
    [params.id]: data,
  };
};
export const UPDATE_YOUTUBE_SWIPE = (state, { data, params }) => {
  state.youtubeSwipe = {
    ...state.youtubeSwipe,
    [params.id]: data,
  };
};

export const UPDATE_EXERCISE_STATUS = (state, params) => {
  const index = state.exerciseList
    .findIndex(item => (item.type.toLowerCase() === params.type.toLowerCase() && item.id === params.id));
  const item = JSON.parse(JSON.stringify(state.exerciseList[index]));
  item.ready = true;
  item.answer = params.answer;
  state.exerciseList.splice(index, 1, item);
  state.exerciseList = JSON.parse(JSON.stringify(state.exerciseList));
};

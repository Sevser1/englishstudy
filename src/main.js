// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'es6-promise/auto';

import Vue from 'vue';
import VueMaterial from 'vue-material';
import Vue2TouchEvents from 'vue2-touch-events';

import 'vue-material/dist/theme/default.css';
import 'vue-material/dist/vue-material.min.css';

import App from './App';
import router from './router';
import store from './store';

Vue.use(VueMaterial);
Vue.use(Vue2TouchEvents);

Vue.config.productionTip = false;

Object.defineProperty(Vue.prototype, '$bus', {
  get() {
    return this.$root.bus;
  },
});

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  data: {
    bus: new Vue({}),
  },
  render: h => h(App),
}).$mount('#app');
